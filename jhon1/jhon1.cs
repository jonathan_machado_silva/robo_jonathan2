﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robocode;
using System.IO;
using System.Security.Permissions;
using System.Drawing;
using Robocode.Util;
using System.Numerics;

namespace nvl.jhon1
{
    public class jhon1 : Robot
    {
        int ESCAPE = 0;
        bool ACAO_INICIAL = false;
        bool ALVO = false;

        public override void Run()
        {


            while (true)
            {
                if (ACAO_INICIAL == false)
                {
                    TurnGunRight(40);
                    Ahead(25);
                }
                else
                {
                    TurnRadarRight(15);
                    Scan();
                }
            }

            //base.Run();
        }

        public override void OnScannedRobot(ScannedRobotEvent e)
        {
            Stop(true);
            double offset = 0;

            if (e.Bearing < 0)
            {
                offset = 360 + e.Bearing + Heading;
            }
            else
            {
                offset = e.Bearing + Heading;
            }


            SetGunForHit(offset);



            if (e.Distance < 400)
            {
                Fire(Rules.MAX_BULLET_POWER);
            }
            else
            {
                Fire(1);
            }

        }


        public override void OnBulletMissed(BulletMissedEvent e)
        {

            ALVO = false;

        }


        public override void OnBulletHit(BulletHitEvent e)
        {
            if (!ACAO_INICIAL)
            {
                ACAO_INICIAL = true;
                SetTankForCorner(e.Bullet.Heading);
            }


            double gunFactor = e.Bullet.Heading - GunHeading;
            if ((gunFactor > -1 && gunFactor < 1) && ALVO)
            {
                if (GetGunHeat() == 0)
                    Fire(Rules.MAX_BULLET_POWER);
            }
            else
                SetGunForHit(e.Bullet.Heading);
        }


        private void SetTankForCorner(double bulletHeading)
        {
            double offsetL = -this.Heading + bulletHeading;

            while (Math.Abs(offsetL) > 360)
                if (offsetL < 0)
                    offsetL += 360;
                else
                    offsetL -= 360;

            if (offsetL < 0)
                offsetL = 360 + offsetL;

            if ((offsetL >= 0 && offsetL < 2) || (offsetL > 358 && offsetL <= 360))
            {
                return;
            }

            if (offsetL >= 0 && offsetL < 180)
                TurnRight(offsetL - 90);
            else
                TurnLeft(-offsetL - 90);


        }

        private void SetGunForHit(double bulletHeading)
        {
            double offsetL = -this.GunHeading + bulletHeading;

            while (Math.Abs(offsetL) > 360)
                if (offsetL < 0)
                    offsetL += 360;
                else
                    offsetL -= 360;

            if (offsetL < 0)
                offsetL = 360 + offsetL;

            if ((offsetL >= 0 && offsetL < 2) || (offsetL > 358 && offsetL <= 360))
            {
                ALVO = true;
                return;
            }

            if (offsetL >= 0 && offsetL < 180)
                TurnGunRight(offsetL);
            else
                TurnGunLeft(360 - offsetL);


            ALVO = true;
        }

        public override void OnHitByBullet(HitByBulletEvent e)
        {
            if (!ACAO_INICIAL)
            {
                ACAO_INICIAL = true;
                SetTankForCorner(e.Heading + 180);
            }



            SetGunForHit(e.Heading + 180);
            Fire(1);

            if (ESCAPE == 0)
            {
                Back(200);
                ESCAPE = 1;
            }
            else
            {
                Ahead(200);
                ESCAPE = 0;
            }

            ALVO = false;
            Scan();

        }

        public override void OnHitWall(HitWallEvent e)
        {
            ALVO = false;

            if ((e.Bearing >= -90 && e.Bearing <= 0) || (e.Bearing >= 0 && e.Bearing <= 90))
                Back(200);
            else
                Ahead(200);

            TurnRight(15);
        }

        #region tools
        private double GetGunHeat()
        {
            return this.GunHeat;
        } 
        #endregion


    }
}

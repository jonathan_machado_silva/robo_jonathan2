README.md
    //Variaveis de apoio: 
    int ESCAPE = 0; //usada na fução de como escapar apos ser atingido
    bool ACAO_INICIAL = false; //usada na função de movimenos iniciais e ação ao encostar na parede se for verdadeiro não  
                               //precisa fazer o loop de ações porque ha um tiroteio.
    bool ALVO = false; //verdadeiro se o inimigo estiver a vista

    public override void Run()
    {


            //ação continua de repetição: 
        while (true)
        {
            if (ACAO_INICIAL == false)  //se movera pela fronteira executando essas duas ações:
            {
                TurnGunRight(40);  //andar para frente 40px
                Ahead(25);         //girar o canhão em 25 graus
            }
            else
            {
                TurnRadarRight(15);
                Scan();
            }
        }

        //base.Run();
    }

    public override void OnScannedRobot(ScannedRobotEvent e) //quando encontrar outro robô execulta as ações a seguir:
    {
        Stop(true); 
        double offset = 0;

        if (e.Bearing < 0) //e.Bearing é a posição relativa do inimigo em relação ao meu tanque
        {
            offset = 360 + e.Bearing + Heading;
        }
        else
        {
            offset = e.Bearing + Heading;
        }


        SetGunForHit(offset);



        if (e.Distance < 400) //e.Distance é a distancia do inimigo ao meu tanque
        {
            Fire(Rules.MAX_BULLET_POWER); //distancia curta ira atirar com maior força
        }
        else
        {
            Fire(1); caso contrario atirar com menor intecidade para não correr risco de perder muita energia.
        }

    }


    public override void OnBulletMissed(BulletMissedEvent e)
    {

        ALVO = false;

    }


    public override void OnBulletHit(BulletHitEvent e)
    {
        if (!ACAO_INICIAL)
        {
            ACAO_INICIAL = true;
            SetTankForCorner(e.Bullet.Heading);
        }


        
        
        // se o alvo estiver no mesmo lugar do acerto, atire novamente com a maior força

        double gunFactor = e.Bullet.Heading - GunHeading;
        if ((gunFactor > -1 && gunFactor < 1) && ALVO)
        {
            if (GetGunHeat() == 0)
                Fire(Rules.MAX_BULLET_POWER);
        }
        else
            SetGunForHit(e.Bullet.Heading);
    }

     //coloca o tanque perpendicular à linha de fogo (para fugir) caso atingido execulta as acões:
    private void SetTankForCorner(double bulletHeading)
    {
        double offsetL = -this.Heading + bulletHeading;

        while (Math.Abs(offsetL) > 360)
            if (offsetL < 0)
                offsetL += 360;
            else
                offsetL -= 360;

        if (offsetL < 0)
            offsetL = 360 + offsetL;

        if ((offsetL >= 0 && offsetL < 2) || (offsetL > 358 && offsetL <= 360))
        {
            return;
        }

        if (offsetL >= 0 && offsetL < 180)
            TurnRight(offsetL - 90);
        else
            TurnLeft(-offsetL - 90);


    }

    //define a arma para a posição correta em direção ao alvo que atira.

    private void SetGunForHit(double bulletHeading)
    {
        double offsetL = -this.GunHeading + bulletHeading;

        while (Math.Abs(offsetL) > 360)
            if (offsetL < 0)
                offsetL += 360;
            else
                offsetL -= 360;

        if (offsetL < 0)
            offsetL = 360 + offsetL;

        if ((offsetL >= 0 && offsetL < 2) || (offsetL > 358 && offsetL <= 360))
        {
            ALVO = true;
            return;
        }

        if (offsetL >= 0 && offsetL < 180)
            TurnGunRight(offsetL);
        else
            TurnGunLeft(360 - offsetL);


        ALVO = true;
    }

    //evento quando atingido:

    public override void OnHitByBullet(HitByBulletEvent e)
    {
        if (!ACAO_INICIAL)
        {
            ACAO_INICIAL = true;
            SetTankForCorner(e.Heading + 180);
        }


        //configuração da arma
        SetGunForHit(e.Heading + 180);
        Fire(1);

        if (ESCAPE == 0)
        {
            Back(200);
            ESCAPE = 1;
        }
        else
        {
            Ahead(200);
            ESCAPE = 0;
        }

        ALVO = false;
        Scan();

    }
        //evento caso atinja a parede 
    public override void OnHitWall(HitWallEvent e)
    {
        ALVO = false;

        if ((e.Bearing >= -90 && e.Bearing <= 0) || (e.Bearing >= 0 && e.Bearing <= 90))
            Back(200);
        else
            Ahead(200);

        TurnRight(15);
    }
Criar uma lógica para esse jogo foi uma grande experiencia para aprimorar as habilidades de busca de informações e resolução de problemas, nunca avia jogado antes, conhecia o git mas não usava o gitLab tudo isso foi uma aprendizagem e um grande desafio, independente do resultado fiz o melhor que pude para concluir esse desafio que me empolgou muito em sua resolução. O maior desafio sem dúvida foi desenvolver algo fora da sua zona de conforto.

Pontos fortes e fracos do robô- a codificação segue uma linha básica de ações sem grandes estratégias multiplayer, ou seja, a jogabilidade duo 1X1 do robô é melhor que a multiplayer talvez esse seja seu ponto fraco. Seu ponto forte é o equilíbrio da força do tiro, pois possui ações tanto com o alvo próximo quanto distante diminuindo assim o risgo de gasto de energia atirando em alvos errados.